<?php
declare(strict_types=1);

namespace MVQN\Localization\Exceptions;


final class DictionaryException extends \Exception
{
}
